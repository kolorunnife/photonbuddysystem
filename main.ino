int waveLed = D7;
int patLed = D0;

void flashLed(int led, int flashDelay, int flashes){
    //First Flash
    for (int i = 0; i < flashes; i++){
        digitalWrite(led, HIGH);
        delay(flashDelay);
        digitalWrite(led, LOW);
        delay(flashDelay);
    }
}

void myHandler(const char *event, const char *data){
    if (strcmp(data, "wave")){
        flashLed(waveLed, 250, 3);
    }
    
    if (strcmp(data, "pat")){
        flashLed(patLed, 150, 5);
    }
}
    

void setup() {
    pinMode(waveLed, OUTPUT);
    pinMode(patLed, OUTPUT);
    
    Particle.subscribe("Deakin_RIOT_SIT210_Photon_Buddy", myHandler);
}

void loop() {

}